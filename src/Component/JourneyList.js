import React from 'react'

export let list = {
    "journeyList" : [
        {
            "id" : 1,
            "day": "Tuesday, Oct 02",
            "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
            "visitingPlace" : [
                {
                  place:  "Visit Sensoji Temple and Asakusa Shrine"
                } ,
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "Explore Akihabara, the Electronics District"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
            ],
            "hotelName" : "Mitsui Garden hotel Otemachi"
        },
        {
            "id" : 2,
            "day": "Tuesday, Oct 03",
            "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
            "visitingPlace" : [
                {
                  place:  "Visit Sensoji Temple and Asakusa Shrine"
                } ,
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "Explore Akihabara, the Electronics District"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
            ],
            "hotelName" : "Mitsui Garden hotel Otemachi"
        },
        {
            "id" : 3,
            "day": "Tuesday, Oct 04",
            "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
            "visitingPlace" : [
                {
                  place:  "Visit Sensoji Temple and Asakusa Shrine"
                } ,
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "Explore Akihabara, the Electronics District"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
            ],
            "hotelName" : "Mitsui Garden hotel Otemachi"
        },
        {
            "id" : 4,
            "day": "Tuesday, Oct 05",
            "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
            "visitingPlace" : [
                {
                  place:  "Visit Sensoji Temple and Asakusa Shrine"
                } ,
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "Explore Akihabara, the Electronics District"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
            ],
            "hotelName" : "Mitsui Garden hotel Otemachi"
        },
        {
            "id" : 5,
            "day": "Tuesday, Oct 06",
            "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
            "visitingPlace" : [
                {
                  place:  "Visit Sensoji Temple and Asakusa Shrine"
                } ,
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "Explore Akihabara, the Electronics District"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
            ],
            "hotelName" : "Mitsui Garden hotel Otemachi"
        },
        {
            "id" : 6,
            "day": "Tuesday, Oct 07",
            "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
            "visitingPlace" : [
                {
                  place:  "Visit Sensoji Temple and Asakusa Shrine"
                } ,
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "Explore Akihabara, the Electronics District"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
                {
                    place:  "See a fun street of Realistic Plastic Food & Buy Japanese Pottery…"
                },
            ],
            "hotelName" : "Mitsui Garden hotel Otemachi"
        }
    ]

}

export let mainContent = {
    "mainContent": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    "mainTitle" : "Tokyo",
    "dateDetails": "October 02-08",
    "place" : "City Tips & Ettiquette"
}