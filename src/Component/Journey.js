import React from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider';
import { Timeline, TimelineItem }  from 'vertical-timeline-component-for-react';

export const Journey = (props) => {
    return(
    <Card className="card-container">
        <Grid container spacing={12} className="journey-container">
        <Grid item xs={12} sm={6} lg={4} xl={3} >
                <div className="journey-title">
                    Day {props.journey.id}
                </div>
                <div className="journey-heading">
                        {props.journey.day}
                </div>
                <div className="journey-content">
                        {props.journey.content}
                </div>
            </Grid>
            <Grid item xs={3}>
                  <img class="place-to-visit"/>
            </Grid>
        </Grid>
        <CardContent>
            <div>
            
                    {props.journey.visitingPlace.map((placeName,index) => (
                        <div key={index}>
                        <Timeline lineColor={'#ddd'}>
                            <TimelineItem
                                key="001">
                                <div className="journey-place">  
                                {placeName.place}
                                </div>
                                <Divider />
                                </TimelineItem>
                            </Timeline>
                            </div>
                    ))
                    }
               
            </div>
            <div className="hotel-name">
                    {props.journey.hotelName}
            </div>
            <div className="header-place fltRyt">
                View Day >
            </div>
        </CardContent>
    </Card>
    )
    
}