import React from 'react';
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent';
import {list} from './JourneyList'
import {mainContent} from './JourneyList'
import {Journey} from './Journey'
import Button from '@material-ui/core/Button';

 class MainContent extends React.Component {
     constructor(props){
         super(props);
         this.state = {
             'planList' : this.props.details.journeyList
         }
     }
     loadList = () =>{
         let tempList = this.props.details.journeyList;
        this.props.details.journeyList.map(elem => {
            tempList.push(elem);
        });
        this.setState({'planList': tempList});
     }
    render() {
        console.log(this.state.planList);    
        return (
            <div >
                <Card className="main-content">
                    <CardContent>
                        <Grid container spacing={32}>
                            { this.props.details.journeyList.map((journeyDetails,index) => (
                                        <Grid item xs={3} sm={4} lg={4} xl={3} key={index}>
                                            <Journey journey={journeyDetails}/>
                                        </Grid>
                            ))
                            }
                        </Grid>
                    </CardContent>
                    <Button variant="contained" className ="btn-grp" color="secondary" onClick={this.loadList} >
                          See More
                    </Button>
                </Card>
            </div>
        )
    }

}

export default MainContent;