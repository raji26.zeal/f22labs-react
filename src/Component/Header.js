import React from 'react';
import Grid from '@material-ui/core/Grid';
import {mainContent} from './JourneyList'
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
export class Header extends React.Component {
 render(){
     const header = mainContent;
    return (
        <Card>
            <CardContent className="card-content-header">
                <div>
                     <Grid container spacing={16}>
                        <Grid item xs={6} className="header-container">
                            <Typography className="header" gutterBottom >
                                {header.mainTitle}
                            </Typography>
                            <Typography gutterBottom className="subTitle" variant="subtitle1">
                                {header.dateDetails}
                            </Typography>
                            <Typography gutterBottom className="main-title-content" variant="subtitle1">
                                {header.mainContent}
                            </Typography>
                            <div className="header-place">
                                {header.place}
                            </div>
                        </Grid>
                        <Grid item xs={6} sm container>
                            <img  className="bg-image" />
                        </Grid>
                    </Grid>
                </div>
            </CardContent>
        </Card>
    )
 }
}